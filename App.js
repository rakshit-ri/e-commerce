// React Native Bottom Navigation
// https://aboutreact.com/react-native-bottom-navigation/
 
import 'react-native-gesture-handler';
 
import * as React from 'react';
 
import
 MaterialCommunityIcons
from 'react-native-vector-icons/MaterialCommunityIcons';
 
import {
  NavigationContainer
} from '@react-navigation/native';
import {
  createStackNavigator
} from '@react-navigation/stack';
import {
  createBottomTabNavigator
} from '@react-navigation/bottom-tabs';
 
import HomeScreen from './pages/HomeScreen.jsx';
import DetailsScreen from './pages/DetailsScreen.jsx';
import ProfileScreen from './pages/ProfileScreen.jsx';
import SettingsScreen from './pages/SettingsScreen.jsx';
 
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
 
function HomeStack() {
  return (
      <Stack.Navigator
        initialRouteName="MyStore"
        screenOptions={{
          headerStyle: { backgroundColor: '#42f44b' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen
          name="MyStore"
          component={HomeScreen}
          options={{ title: 'My Store' }}/>
        <Stack.Screen
          name="Details"
          component={DetailsScreen}
          options={{ title: 'Details Page' }} />
      </Stack.Navigator>
  );
}
 
function SettingsStack() {
  return (
    <Stack.Navigator
      initialRouteName="Settings"
      screenOptions={{
        headerStyle: { backgroundColor: '#42f44b' },
        headerTintColor: '#fff',
        headerTitleStyle: { fontWeight: 'bold' }
      }}>
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={{ title: 'Setting Page' }}/>
      <Stack.Screen
        name="Details"
        component={DetailsScreen}
        options={{ title: 'Details Page' }}/>
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ title: 'Profile Page' }}/>
    </Stack.Navigator>
  );
}
 
function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Feed"
        tabBarOptions={{
          activeTintColor: '#42f44b',
        }}>
        <Tab.Screen
          name="MyStore"
          component={HomeStack}
          options={{
            tabBarLabel: 'MyStore',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="MyStore"
                color={color}
                size={size}
              />
            ),
          }}  />

        <Tab.Screen
          name="Product Inventory"
          component={HomeStack}
          options={{
            tabBarLabel: 'Inventory',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="inventory"
                color={color}
                size={size}
              />
            ),
          }}  />
        <Tab.Screen
          name="SettingsStack"
          component={SettingsStack}
          options={{
            tabBarLabel: 'Settings',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="settings"
                color={color}
                size={size}
              />
            ),
          }} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
export default App;
